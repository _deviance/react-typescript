import * as React from 'react'

interface PokemonProps {
  name: string,
  pNumber: number,
  image: string
}

class Pokemon extends React.Component<PokemonProps, any> {

  constructor(props: any) {
    super(props)
  }

  render() {
    let { name, pNumber, image } = this.props
    return (
      <div>
        <h1 style={{textTransform: 'capitalize'}}>{name}</h1>
        <p>Pokemon #: {pNumber}</p>
        <img src={image} alt={name}/>
      </div>
    )
  }
}

export default Pokemon
