import * as React from 'react'
import './App.css'

import { GetPokemon } from './api'

import Pokemon from './components/Pokemon'

interface StateInterface {
  textInput: string,
  loading: boolean,
  pokemon?: {
    name: string,
    pNumber: number,
    image: string
  }
}

class App extends React.Component<any, StateInterface> {

  constructor(props: any) {
    super(props)

    this.state = {
      textInput: '',
      loading: false
    }

    this._formSubmit = this._formSubmit.bind(this)
    this._onChange = this._onChange.bind(this)
  }

  _formSubmit(e: any) {
    e.preventDefault()

    this.setState({ loading: true })
    GetPokemon(this.state.textInput).then((data: any) => {
      this.setState({ 
        loading: false, 
        pokemon: {
          name: data.name,
          pNumber: data.id,
          image: data.sprites.front_default
        }
      })
    }).catch((err: any) => {
      this.setState({ loading: false })
      alert(err)
    })
  }

  _onChange(e: any) {
    this.setState({ textInput: e.target.value })
  }

  // componentWillMount() {
    
  // }

  renderLoading() {
    if (this.state.loading) {
      return (
        <p>Fetching your pokemon...</p>
      )
    } else {
      return null
    }
  }

  renderPokemon() {
    if (this.state.pokemon) {
      return (
        <Pokemon 
          {...this.state.pokemon}
        />
      )
    } else {
      return null
    }
  }

  render() {
    let { textInput } = this.state

    return (
      <div className="App">
        <p>Enter a pokemon number (1-802) or name (ex. "Pikachu", "Charizard")</p>
        <form onSubmit={this._formSubmit}>
          <input type="text" value={textInput} onChange={this._onChange} />
          {this.renderLoading()}
        </form>

        {this.renderPokemon()}
      </div>
    )
  }
}

export default App
