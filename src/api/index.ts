import axios from 'axios'
export const GetPokemon = (name: string): any => {
  return new Promise((resolve, reject) => {
    axios.get('https://pokeapi.co/api/v2/pokemon/' + name).then((data: any) => {
        resolve(data.data)
    }).catch(err => {
      reject('Error fetching Pokemon')
    })
  })
}